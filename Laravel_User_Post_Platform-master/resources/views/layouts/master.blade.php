<!DOCTYPE html>
<html>
    <head>
        <title>@yield("title")</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('assets/css/myCSS1.css') }}">
    </head>
    <body>
    	@include("includes.header")
        <div class="container">
            @yield("content")
        </div>

        <script type="text/javascript" src="{{URL::to('assets/js/jquery3.min.js')}}" ></script>
        <script type="text/javascript" src="{{URL::to('assets/js/bootstrap.min.js')}}" ></script>
        <script type="text/javascript" src="{{URL::to('assets/js/myJS1.js')}}"></script>
    </body>
</html>
