@extends("layouts.master")

@section("logout")
	<li><a href="{{route('logout')}}">Logout</a></li>
@endsection

@section("account")
	<li><a href="{{route('accounts')}}"> Account </a></li>
@endsection


@section("content")
	<section class="row new-post">
		<div class="col-md-6 col-md-offset-3">
			<header><h3>What do you have to say ??? </h3></header>
			<form action="{{ route('create_new_post') }}" method="post">
				<div class="form-group">
					<textarea class="form-control" name="body" id="body" rows="5" placeholder="......"></textarea>
				</div>
				<button type="submit" class="btn btn-primary"> Create Post </button>
				<input type="hidden" name="_token" value="{{ Session::token() }}" />
			</form>
		</div>
	</section>
	<section class="row posts">
		<div class="col-md-6 col-md-offset-3">
			<header><h3>What people have said !!!</h3></header>
			@foreach($posts as $post)
				<article class="post" data-postid="{{$post->id}}"> 
					<p> {{ $post->body }} </p> 
					<div class="info">
						Posted by {{$post->user__data->name}} on {{ $post->created_at }}.
					</div>
					<div class="interaction">
						<a href="#" class="react like">{{Auth::user()->likes()->where("post_id",$post->id)->first() == null ? "Like" : (Auth::user()->likes()->where("post_id",$post->id)->first()->reaction == 1 ? "Liked" : "Like")}}</a> |
						<a href="#" class="react dislike">{{Auth::user()->likes()->where("post_id",$post->id)->first() == null ? "DisLike" : (Auth::user()->likes()->where("post_id",$post->id)->first()->reaction == 0 ? "DisLiked" : "DisLike")}}</a>
						@if(Auth::user() == $post->user__data)
						|
						<a href="#" class="edit">Edit</a> |
						<a class="delete" href="{{route('post.delete',['postID' => $post->id])}}">Delete</a>
						@endif
					</div>
				</article>
			@endforeach
		</div>
	</section>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    	<div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Edit Post</h4>
	      </div>
	      <div class="modal-body">
	        		<div class="form-group">
						<textarea class="form-control" name="body" id="editPostBody" rows="5" placeholder="......"></textarea>
					</div>
			</div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    <button type="submit" class="btn btn-primary" id="edit_modal_save"> Save Changes </button>
	  	  </div>
	    </div>
  </div>
</div>

<script type="text/javascript">
	var token = "{{Session::token()}}",
		urlEditPost = "{{route('editPost')}}",
		urlReact = "{{route('reaction')}}";
</script>

@endsection