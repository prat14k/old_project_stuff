@extends("layouts.master")

@section("title")
	Account
@endsection

@section("logout")
	<li><a href="{{route('logout')}}">Logout</a></li>
@endsection

@section("account")
	<li><a href="{{route('accounts')}}">Account</a></li>
@endsection


@section("content")
	<section class="row new-post">
		<div class="col-md-6 col-md-offset-3">
			<header><h3>Your Account</h3></header>
			<form action="{{route('account.save')}}" method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label for="name">First Name</label>
					<input type="text" name="name" class="form-control" value="{{$user->name}}" id="name">
				</div>
				<div class="form-group">
					<label for="image">Image(only jpeg)</label>
					<input type="file" name="image" class="form-control" value="{{$user->name}}" id="image">
				</div>
				<input type="hidden" name="_token" value="{{Session::token()}}">
				<button type="submit" class="btn btn-primary">Save Settings</button>
			</form>
		</div>
	</section>
	@if(Storage::disk('local')->has($user->name . '_'.$user->id.".jpg"))
		<section class="row new-post">
			<div class="col-md-6 col-md-offset-3">
				<img src="{{route('account.image',['filename' =>  $user->name.'_'.$user->id.'.jpg'])}}" alt="" class="img-responsive">
			</div>
		</section>
	@endif
@endsection