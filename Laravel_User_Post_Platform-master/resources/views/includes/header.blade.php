<header>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav_contents">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{route('dashboard')}}">UConnect  </a>
			</div>
			<div class="collapse navbar-collapse" id="nav_contents">
				<ul class="nav navbar-nav navbar-right">
					@yield("logout")
					@yield("account")
				</ul>
			</div>
		</div>
	</nav>
</header>

<div class="container">
	    @include("includes.message")
</div>