@extends("layouts.master")

@section("title")
    Welcome
@endsection

@section("projectName")
    UConnect
@endsection

@section("content")

<div class="row">
	<div class="col-md-6">
		<h3> Signup</h3>
		<form action="{{ route('signup')}}" method="post">
			<div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
				<label for="#email">Email-ID</label>
				<input type="text" name="email" id="email" class="form-control" value="{{ Request::old('email') }}" />	        				
			</div>
			<div class="form-group {{$errors->has('username') ? 'has-error' : ''}}">
				<label for="#username">Username</label>
				<input type="text" name="username" id="username" class="form-control" value="{{ Request::old('username') }}"/>	        				
			</div> 
			<div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
				<label for="#name">Name</label>
				<input type="text" name="name" id="name" class="form-control" value="{{ Request::old('name') }}"/>	        				
			</div> 
		 	<div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
				<label for="#password">Password</label>
				<input type="password" name="password" id="password" class="form-control" value="{{ Request::old('password') }}"/>	        				
			</div>
			<div class="form-group">
				<input type="submit" name="submit" id="submit" value="Signup" class="btn btn-primary" />
			</div> 
			<input type="hidden" name="_token" value="{{ Session::token() }}">
		</form>
	</div>
	<div class="col-md-6">
		<h3> Signin </h3>
		<form action="{{ route('login') }}" method="post">
			<div class="form-group {{$errors->has('username') ? 'has-error' : ''}}">
				<label for="#username">Username</label>
				<input type="text" name="username" id="username" class="form-control" />	        				
			</div>
		 	<div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
				<label for="#password">Password</label>
				<input type="password" name="password" id="password" class="form-control" />	        				
			</div>
			<div class="form-group">
				<input type="submit" name="submit" id="submit" value="Signin" class="btn btn-info" />
			</div>
			<input type="hidden" name="_token" value="{{ Session::token() }}">  
		</form>
	</div>
</div>
@endsection