<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	public function user__data(){
		return $this->belongsTo("App\User_Data");
	}


    public function likes(){
    	return $this->hasMany("App\Like");
    }

}
