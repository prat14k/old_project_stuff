<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    public function user__data(){
    	return $this->belongsTo("App\User_Data");
    }

    public function post(){
    	return $this->belongsTo("App\Post");
    }
}
