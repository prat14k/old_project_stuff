<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller{

	public function getDashboard(){

		$posts = Post::orderBy("created_at","desc")->get();//Post::all(); //to get all posts
		return view("dashboard",[
				'posts' => $posts
				]);
	}

	public function postCreatePost(Request $req){

		$this->validate($req,[
			"body" => "required"
			]);
		$post = new Post();
		$post->body = $req["body"];
		$message = "Unable to save";
		
		if ($req->user()->posts()->save($post)) {
			$message = "Created Successfully";
		}
		return redirect()->route("dashboard")->with(["message" => $message]);
	}

	public function getDeletePost($postID){
		$post = Post::where("id",$postID)->first();
		//could use get() instead of first() but it will also return an array
		// could use find($postID) instead of where() clause
		//if yoou want to compare it, thrn pass it as the second arg in the where() clause, making it of three arguments in total

		if(Auth::user() != $post->user__data)
			return redirect()->back();

		$post->delete();
		return redirect()->route("dashboard")->with(["message" => "Deleted Successfully"]);

	}

	public function logout(){
		Auth::logout();
		return redirect()->route("home");
	}

	public function posteditpost(Request $req){

		$this->validate($req,[
			"body" => "required"
			]);


		$post = Post::find($req['postID']);
		
		if(Auth::user() != $post->user__data)
			return response()->json(["message" => "Not your post"]);

		$post->body = $req['body'];

		$post->update();

		return response()->json(["message" => "Updated successfully"],200);

	}

}

