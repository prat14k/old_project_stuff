<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Like;
use App\Post;

class LikeController extends Controller{

	public function saveReaction(Request $req){

		$userReaction = $req['reaction'];
		$postID = $req['postID'];
		if(($userReaction!=1)&&($userReaction!=-1)){
			return response()->json(["error" => true , "message" => "Incorrect reaction values passed"]);
		}
		//return response()->json(["message" => "Correct reaction values passed ".$postID]);
		$value = $userReaction;
		$userReaction = ($userReaction == 1);

		$posts = Post::find($postID);
		if(!$posts)
			return response()->json(["error" => true , "message" => "Wrong Post ID value passed"]);	

		$update = false;
		$user = Auth::user();
		$react = $user->likes()->where("post_id",$postID)->first();

		
		if($react){
			$update = true;
			if($react->reaction == $userReaction){
				$react->delete();
				return response()->json(["error" => false , "message" => "Successful->D" , "val" => "0"]);
			}
		}
		else{
			$react = new Like();
		}



		$react->reaction = $userReaction;
		$react->user__data_id = $user->id;
		$react->post_id = $postID;

		if($update){
			$react->update();
		}
		else{
			$react->save();
		}

		return response()->json(["error" => false , "message" => "Successful" , "val" => $value]);

	}

}
