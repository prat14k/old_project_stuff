<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User_Data;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class UserController extends Controller{

	
	public function postSignUp(Request $request){
		
		$this->validate($request, [
			"email" => "required|email",
			"name" => "required|max:120",
			"username" => "required|min:3|unique:user__datas",
			"password" => "required|min:4"
		]);
		
		$user = new User_Data();
		$user->email = $request['email'];
		$user->name = $request["name"];
		$user->username = $request["username"];
		$user->password = bcrypt($request["password"]);

		$user->save();

		Auth::login($user);

		return redirect()->route("dashboard");
	}

	public function postSignIn(Request $request){
		

		$this->validate($request, [
			"username" => "required",
			"password" => "required"
		]);

		$username = $request["username"];
		$password = $request["password"];

		if(Auth::attempt(["username" => $username , "password" => $password])){
			return redirect()->route("dashboard");
		}

		return redirect()->back();

	}

	public function loadAccount(){
		return view("account",["user" => Auth::user()]);
	}

	public function updateUserAccount(Request $req){
		$this->validate($req,[
			'name' => "required|max:250|min:3"
			]);

		$user = Auth::user();
		$user->name = $req['name'];
		$user->update();

		$file = $req->file('image');
		$filename = $req['name'].'_'.$user->id.".jpg";

		if($file){
			Storage::disk('local')->put($filename,File::get($file));
		}

		return redirect()->route("accounts");

	}

	public function loadImage($filename){
		$file = Storage::disk('local')->get($filename);
		return new Response($file,200);
	}

}