<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
	This routes group applies the "web" middleware  group to every route it contains.The "web" middleware group is defined in your HTTP kernel and includes session state,CSRF protection , and more.

*/  

Route::group(["middleware" => ["web"]],function(){

	Route::post("/reaction",[
		"uses" => "LikeController@saveReaction",
		"as" => "reaction",
		"middleware" => "auth"
		]);

	Route::get('/', function () {
		return view('welcome');
	})->name("home");

	Route::post("/edit_post",[
			"uses" => "PostController@posteditpost",
			"middleware" => "auth"
		])->name("editPost");

	Route::post("/createPost",[
			"uses" => "PostController@postCreatePost",
			"as" => "create_new_post",
			"middleware" => "auth"
		]);

	Route::post("/signup",[
		"uses" => "UserController@postSignUp",
		"as" => "signup"
		]);

	Route::get("/dashboard",[
		"uses" => "PostController@getDashboard",
		"as" => "dashboard" ,
		"middleware" => "auth"
		]);

	Route::post("/login",[
		"uses" => "UserController@postSignIn",
		"as" => "login"
		]);
	Route::get("/deletePost/{postID}",[
		"uses" => "PostController@getDeletePost",
		"as" => "post.delete",
		"middleware" => "auth"
		]);
	Route::get("/logout",[
		"uses" => "PostController@logout",
		"as" => "logout",
		"middleware" => "auth"
		]);

	Route::get("/account",[
		"uses" => "UserController@loadAccount",
		"as" => "accounts"
		])->middleware("auth");

	Route::post("/updateAccount",[
		"uses" => "UserController@updateUserAccount",
		"as" => "account.save",
		"middleware" => "auth"
		]);

	Route::get("/userImage/{filename}",[
		"uses" => "UserController@loadImage",
		"as" => "account.image"
		]);

});

