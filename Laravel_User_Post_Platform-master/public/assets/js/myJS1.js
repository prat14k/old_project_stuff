var postID=0;
var postEditElement = null; 
$(".edit").on("click",function(event){
	event.preventDefault();
	//console.log(event.target.parentNode.parentNode.childNodes[1]);
	postEditElement = event.target.parentNode.parentNode.childNodes[1];
	var postBody = postEditElement.innerHTML;
	$("#myModal").modal();
	$("#editPostBody").val(postBody);
	postID = event.currentTarget.parentNode.parentNode.dataset['postid'];
});

$("#edit_modal_save").on("click",function(){

			var postContent = $("#editPostBody").val();
			if(postContent == ""){
				return;
			}

			$.ajax({
                url: urlEditPost,
                headers: {'X-CSRF-TOKEN': token},
                data: { '_token' : token , 'postID' : postID , 'body' : postContent},
                type: 'POST',
                datatype: 'JSON',
                success: function (resp) {
                    console.log(resp["message"]);
                    postEditElement.innerHTML = postContent;
                    $("#myModal").modal("hide");
                	}
            });
});


$(".react").on("click",function(event){
	event.preventDefault();
	var clickedElement = event.target;
	var val = 0;
	if(clickedElement.classList.contains("like")){
		val = 1;
	}
	else if(clickedElement.classList.contains("dislike")){
		val = -1;
	}
	else {
		return;
	}

	postID = clickedElement.parentNode.parentNode.dataset["postid"];

	//console.log(postReactID + " " + urlReact);


	$.ajax({
		url: urlReact,
		headers: {'X-CSRF-TOKEN' : token},
		data: {
			'_token' : token,
			'reaction' : val,
			"postID" : postID
		},
		type: 'POST',
		datatype: 'JSON',
		success: function(resp){
			if(resp.error == false){
				if(resp.val == "0"){
					if(val==1)
						clickedElement.innerHTML = "Like";
					else
						clickedElement.innerHTML = "DisLike";
				}
				else if(resp.val == "1"){
					clickedElement.innerHTML = "Liked";
					clickedElement.parentNode.childNodes[3].innerHTML = "DisLike";	
				}

				else if(resp.val == "-1"){	
					clickedElement.parentNode.childNodes[1].innerHTML = "Like";
					clickedElement.innerHTML = "DisLiked";
				}
			}
		}
	});


});