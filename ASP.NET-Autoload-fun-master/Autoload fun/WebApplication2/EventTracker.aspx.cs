﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;


namespace WebApplication2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            SetInitialRow();

        }
        private void SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("Column1", typeof(string)));
            dt.Columns.Add(new DataColumn("Column2", typeof(string)));
            dt.Columns.Add(new DataColumn("Column3", typeof(string)));
            dr = dt.NewRow();
            dr["RowNumber"] = 1;
            dr["Column1"] = string.Empty;
            dr["Column2"] = string.Empty;
            dr["Column3"] = string.Empty;
            dt.Rows.Add(dr);
            //dr = dt.NewRow();

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            GridView1.DataSource = dt;
            
            GridView1.DataBind();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            // When the Page.PreRender event occurs, it is too late
            // to change the list.
            Log("Page_PreRender");
        }
        protected void CtrlChanged(Object sender, EventArgs e)
        {
            // Find the control ID of the sender.
            // This requires converting the Object type into a Control class.
            string ctrlName = ((Control)sender).ID;
            Log(ctrlName + " Changed");
        }
        private void Log(string entry)
        {
            lstEvents.Items.Add(entry);
            // Select the last item to scroll the list so the most recent
            // entries are visible.
            lstEvents.SelectedIndex = lstEvents.Items.Count - 1;
        } 
    }
}
