﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EventTracker.aspx.cs" Inherits="WebApplication2.WebForm1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
 
  <title>Untitled Page</title>
  </head>
<body>
    <form id="form1" runat="server">
    <div>
            <h1>Controls being monitored for change events:</h1>
            
            
            <asp:TextBox ID="txt" runat="server" AutoPostBack="true" OnTextChanged="CtrlChanged" />
            <br /><br />
            

        <asp:GridView ID="GridView1" runat="server">
            
        </asp:GridView>



            <br /><br />
            <asp:RadioButton ID="opt1" runat="server" GroupName="Sample"
            AutoPostBack="true" OnCheckedChanged="CtrlChanged"/>
            &nbsp;&nbsp;&nbsp;
            <asp:RadioButton ID="RadioButton1" runat="server" GroupName="Sample"
            AutoPostBack="true" OnCheckedChanged="CtrlChanged"/>
            &nbsp;&nbsp;&nbsp;
            <asp:RadioButton ID="opt2" runat="server" GroupName="Sample"
            AutoPostBack="true" OnCheckedChanged="CtrlChanged"/>
            <h1>List of events:</h1>
            <asp:ListBox ID="lstEvents" runat="server" Width="355px"
             Height="150px" /><br />
            <br /><br /><br />
    </div>
    </form>
</body>
</html> 